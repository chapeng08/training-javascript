const question = document.getElementById("question");
const choices = Array.from(document.getElementsByClassName ("choice-text"));

let currentQuestion = {};
let acceptingAnswers = false;
let score = 0;
let questionCounter = 0;
let availableQuesions = [];

let questions = [
  {
    question: "A triangle choke is a form of",
    choice1: "A strangulation choke",
    choice2: "A blood choke",
    choice3: "An anaconda choke",
    choice4: "A darce choke",
    answer: 2
  },
  {
    question: "When you chicken wing your opponents arm and provide torque to his shoulder with your arms, that move is called...",
    choice1: "Key lock",
    choice2: "Omoplata",
    choice3: "Kimura",
    choice4: "Americana",
    answer: 3
  },
  {
    question: "What are the three pillars of jiu jitsu?",
    choice1: "Triangle, kneebar, kimura",
    choice2: "Armbar, americana, omoplata",
    choice3: "Armbar, triangle, omoplata",
    choice4: "Omoplata, gogoplata, monoplata",
    answer: 3
  },
  {
    question: "Which position can you not get a kneebar from?",
    choice1: "Half guard top",
    choice2: "Half guard bottom",
    choice3: "Bottom mount",
    choice4: "Bottom guard",
    answer: 3
  }
];

//CONSTANTS
const CORRECT_BONUS = 10;
const MAX_QUESTIONS = 4;

startGame = () => {
  questionCounter = 0;
  score = 0;
  availableQuesions = [...questions];
  console.log(availableQuesions);
  getNewQuestion();
};

getNewQuestion = () => {
    if (availableQuesions.length === 0 || questionCounter >= MAX_QUESTIONS) {
        //go to the end page
        return window.location.assign("file:///home/sharafdeen/training-javascript/testQuiz3/end.html");
    }
    questionCounter++;
    const questionIndex = Math.floor(Math.random() * availableQuesions.length);
    currentQuestion = availableQuesions[questionIndex];
    question.innerText = currentQuestion.question;

    choices.forEach(choice => {
        const number = choice.dataset["number"];
        choice.innerText = currentQuestion["choice" + number];
    });

    availableQuesions.splice(questionIndex, 1);
    acceptingAnswers = true;

    };

    choices.forEach(choice => {
        choice.addEventListener("click", e => {
          if (!acceptingAnswers) return;
      
          acceptingAnswers = false;
          const selectedChoice = e.target;
          const selectedAnswer = selectedChoice.dataset["number"];
          
          const classToApply =
            selectedAnswer == currentQuestion.answer ? "correct" : "incorrect";
          
          selectedChoice.parentElement.classList.add(classToApply)
          setTimeout(() => {
            selectedChoice.parentElement.classList.remove(classToApply);
            getNewQuestion();
        }, 1000);
        });
    });

startGame();