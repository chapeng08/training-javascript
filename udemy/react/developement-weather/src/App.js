import React from 'react';
import './Style.css';
import NewsList from './components/newsList';
import Searchbar from './components/searchbar';



class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      baseUrl: 'https://api.nytimes.com/svc/search/v2/articlesearch.json?',
      APIKey: 'RT6TWj8H5GhJWdYreoGx088BYPCDkpgg',
      input: '',
      results: []
  }
}

handleChange = (e) => {
  this.setState({
    input: e.target.value
  })
  //console.log(e.target.value)
}

handleSubmit = () => {
  console.log(this.state.input)
  this.query()
  this.setState({input: ''})
}

query = () => {
  let url = this.state.baseUrl + "q=" + this.state.input + "&api-key=" + this.state.APIKey
  fetch(url).then(response => {
    if(!response.ok){
    console.log(response.statusText)
    return
  }
  return response.json()
  }).then(data => {
    let docs = data.response.docs
    let results = docs.map(doc => { // La methode map permet de faire des iterations pour chaque objet dans une collection.
      let url = doc.web_url
      let headline = doc.headline
      let main = headline.main
      let date = doc.pub_date
      let byline = doc.byline
      let author = byline.original
      let id = doc._id
      return {id: id, title: main, date: date, url: url, author: author}
    })
    this.setState({results: results})
  })
  
}


  render() {
    return (
      <div className="App">
      <Searchbar 
      change={this.handleChange}
      submit={this.handleSubmit}
      text={this.state.input}
      />
      <NewsList results={this.state.results} />
      </div>
    );
  }
}

export default App;
