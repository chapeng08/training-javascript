import React, {Component} from 'react';

export default class NewsList extends Component {
    constructor(props){
        super(props)

    }
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-me-8 offset-2">
                    <ul>
                    {this.props.results.map(result => {
                    return(<li key={result.id}><a href={result.url} target="blank">{result.title}</a></li>) // Dans chaque element d'une collection, nous devons renseigner pour chacun une key.
                    })}
                    </ul>
                    </div>

                </div>

            </div>
        )
    }
}

