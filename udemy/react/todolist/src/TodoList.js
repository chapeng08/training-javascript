import React, { Component } from 'react';


const Header = ({change, add, input, placeholder}) => {
    return (<form onSubmit={(e) => {e.preventDefault() ; add() }}> 
    <input placeholder={placeholder} className="rounded" 
           value={input}
           onChange={(e) => change(e)}/>
    <button className="rounded">Add</button>
    </form>
    )}

const List = ({items, delet}) => {
    return (<ul className="theList">
        {items.map(item => {
        return(<li key={item.key} onClick={() => delet(item.key)}>{item.text}</li>
        )
        })}
        </ul>
    )
}


export default class TodoList extends Component {
    constructor(props) {
        super(props)
        this.state = {
          items: [
            {text: "Learn react", key: Date.now()} //  chaque objet cree de type item, on aura un texte + une clef avec id unique
          ],
          input: '',
          placeholder: 'Enter task'
        }
      }
    
    componentDidMount() {
      let items = JSON.parse(localStorage.getItem("items"))
      this.setState({items: items})
    } 

    handleChange = (e) => { /* via cette methode on veut recuperer la nouvelle valeur dans Input 
      // console.log(e.target.value) */
      this.setState({input: e.target.value})
    }
    
    add = () => {
      // console.log(this.state.input)
    
      let text = !this.state.input.length ? "Attention, input vide." : "Enter task"
      this.setState({placeholder : text})
    
      if(!this.state.input.length) {
        return 
      }
    
      let newItem = {text: this.state.input, key: Date.now()}
      this.setState(state => ({
        items: [newItem].concat(state.items),
        input: ""
      }))
      localStorage.setItem('items', JSON.stringify([newItem].concat(this.state.items)))
    }
    
    delete = (key) => {
      let filtered = this.state.items.filter(item => {
        if(key != item.key) { return item }
      })
      this.setState({
        items : filtered
      })
    localStorage.setItem('items', JSON.stringify(filtered))
    }


    render() {
        return(
            <div id="container">
            <div className="todoListMain">
            <div className="header">
                <Header 
                change={this.handleChange}
                add = {this.add}
                input = {this.state.input}
                placeholder = {this.state.placeholder}
                />
                <List
                items = {this.state.items}
                delet = {this.delete}
                />
            </div>
            </div>
            </div>
        );
    }
}

/* map pour faire literation qui prendra deux arguments: un item qui correspondera a chaque objet dans cette collection puis une fonction pour l'execution de l'operation pour chaque objet. */