import React from 'react';
import './App.css'; // Importer les fichiers/composants pour avoir acces dans notre fichier.


class App extends React.Component { // On cree tout d'abord notre premier et principal composant.
  constructor(props) {
    super(props)
    this.state = {
      heading: "Shopping List",
      items: [
        {text:"Go to the training", done: false},
        {text:"Go to the dentist", done: false},
        {text:"Go to the doctor", done: false},
        {text:"Go to the gym", done: false}        
      ],
      message: ""
  }
}

markDone = (todo) => {
  let todos = this.state.items.map(t => {
    if(t.text === todo.text) {
      return Object.assign({}, t, {
        done: !t.done
      })
    }
    return t
  })
  this.setState({items: todos})
}

afficherMessage = () => {
  alert("Bonjour React");
};



  render() {
    return(
      <div id="container">
        <br />
        <div className="row">
          <div className="col-md-6 offset-3">
        <h1>{this.state.heading}</h1>

        <ul>
          {this.state.items.map(item => {
            return(<li className={item.done ? "done" : ""} onClick={() => this.markDone(item)} key={item.text}>{item.text}</li>)
          })
          }
        </ul>
        <div>
        <button onClick={this.afficherMessage}>React</button>
        <h1>{this.state.message}</h1>
        </div>
        </div>
        </div> 
        </div>
    )
  }
}

export default App; // Toujours exporter notre composant afin de le rendre accessible dans toute notre app.