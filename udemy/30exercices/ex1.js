var a = 1;
var b = 5;

console.log("----------Avant Inversion------------");
console.log("A : " + a);
console.log("B : " + b);

var tmp = a; // variable temporaire = a donc = 1
a=b; // a = 5
b=tmp; // b = variable temporaire donc = 1

console.log("----------Après Inversion------------");
console.log("A : " + a);
console.log("B : " + b);