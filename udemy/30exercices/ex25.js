var classe = {
    marc : {
        nom : "Marc",
        age : 21,
        notes : [12,20,13,10,7],
        },

    eric : {
        nom : "Eric",
        age : 18,
        notes : [5,15,20,8,17]
        },

    marie : {
        nom : "Marie",
        age : 22,
        notes : [10,10,11,20,11],        
        },

    saffya : {
        nom : "Saffya",
        age : 29,
        notes : [3,15,11,20,11],        
        },
    
    afficherEleve : function(eleve){
        console.log("Nom : " + eleve.nom);
        console.log("Age : " + eleve.age);
        console.log("Notes : ");
        var notestxt = "";
        for(var i = 0; i < eleve.notes.length; i++){
            notestxt += "num" + i + " - " + eleve.notes[i] + "\n";
        }
        console.log(notestxt);
    },

    calculMoyenneEleve : function(eleve){
        var moyenne = 0;
        for(var i = 0; i < eleve.notes.length; i++){
        moyenne += eleve.notes[i];
        };
        return moyenne / eleve.notes.length;
    },

    afficherClasse : function(){
        this.afficherEleve(this.marc);
        this.afficherEleve(this.eric);
        this.afficherEleve(this.marie);
        this.afficherEleve(this.saffya);

    },

    calculMoyenneClasse : function(){
        var moyenneClasse = 0;
        moyenneClasse += this.calculMoyenneEleve(this.marc);
        moyenneClasse += this.calculMoyenneEleve(this.eric);
        moyenneClasse += this.calculMoyenneEleve(this.marie);
        moyenneClasse += this.calculMoyenneEleve(this.saffya);
        
        return moyenneClasse / 4;
    }

    };


console.log("Voici vos eleves : \n");
classe.afficherClasse();
console.log(`Voici la moyenne de la classe: ${classe.calculMoyenneClasse()}`);
