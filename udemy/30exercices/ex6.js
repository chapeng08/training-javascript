var readline = require ("readline-sync");

var value = parseInt(readline.question("Quel chiffre voulez vous saisir ? "));
var stop = false;


while(!stop) {
    if(isNaN(value)){
        console.log("Veuillez recommencer, votre saisie n'est pas un chiffre.");
        value = parseInt(readline.question("Quel nombre voulez vous saisir ? "));
    } else {
        console.log("Le chiffre saisi est : " + value);
        stop = true;
    }
}