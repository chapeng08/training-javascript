var readline = require ("readline-sync");
var armesManager = require ("./ex28ArmesManager.js");


var saisie = -1;
while(saisie !== 0){
    menu()
    saisie = parseInt(readline.question("Quel est votre choix ? "));
    switch(saisie){
        case 1 : armesManager.afficherArmes();
        break;
        case 2 : armesManager.ajouterArme();
        break;
        case 3 : armesManager.supprimerArme();
        break;
        case 0 : console.log("A + ");
        break;
        default : console.log("Je n'ai pas compris");
    };
}

function menu(){
    var txt = "";
    txt += "1/ Afficher les armes \n";
    txt += "2/ Ajouter une arme \n";
    txt += "3/ Supprimer une arme \n";
    txt += "0/ Quitter le programme";
    console.log(txt);
}