const myButton = document.querySelector("button");
const myDiv = document.querySelector("div");

myButton.addEventListener("click", function(){
    myDiv.innerHTML = typeOfNumber();
});

function typeOfNumber(){
    var randomNumber = Math.floor(Math.random() * 20); // arrayName.length permet de traiter plusieurs valeurs
    if(randomNumber <= 7) return randomNumber + "<br /> Petit";
    else if(randomNumber <= 15) return randomNumber + "<br /> Moyen";
    else return randomNumber + "<br /> Grand";
};