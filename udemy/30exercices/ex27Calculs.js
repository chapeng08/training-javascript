var calculs = {
    addition : function(chiffre){
        var resultat = 0;
        for(let i = 1; i <= chiffre; i++){
        resultat += i;
        }
        return resultat;
    },

    factorielle : function(chiffre){
        var resultat = 1;
        for(let i = 1; i <= chiffre; i++){
        resultat *= i;
        }
        return resultat;
    }
};