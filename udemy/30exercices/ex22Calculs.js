var readline = require ("readline-sync");


var calculsManager = {
    addition : function(){
        var saisieNombre1 = parseInt(readline.question("Combien de nombre? "));
        var total = 0;
        for(let i = 1; i <= saisieNombre1; i++){
        total += i;
        console.log("Etape " + i + " : " + total);
        }
        console.log("L'addition des " + saisieNombre1 + " premiers nombres est : " + total);
    },
    multiplication : function(){
        var saisieNombre2 = parseInt(readline.question("Combien de nombre? "));
        var total = 1;
        for(let i = 1; i <= saisieNombre2; i++){
        total *= i;
        console.log("Etape " + i + " : " + total);
        }
        console.log("La multiplication des " + saisieNombre2 + " premiers nombres est : " + total);
    }
}

module.exports = calculsManager;