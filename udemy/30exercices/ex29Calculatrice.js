/* On va recuperer nos Id */
const divTouches = document.querySelector("#touches"); /* # => id */
const divResultat = document.querySelector("#resultat");
const divRecap = document.querySelector("#recap");


var chiffreSaisi = ""; /* on ititialise la valeur du chiffre que l'on saisit a vide */
var operateur = "+";
var recap = "";
var resultat = 0;

var nbCalculs = 0;
var reinit = true;

divTouches.addEventListener("click", function(event){ /* on renseigne "event" pour pouvoir le reutiliser plus tard */
    var button = event.target.id; /* action pour pouvoir recuperer l'Id lorsqu'on clique. Valeur qu'on stocke ensuite dans la variable button */
    if(button.substring(0,1) === "c"){ /* la condition concerne le premier chiffre s'il est egale a c */
        chiffreSaisi += button.substring(1,2); /* Pour recuperer seulement le deuxieme chiffre et += si on a besoin d'un nombre a plusieurs chiffres */
        divResultat.value = chiffreSaisi; /* Pour afficher le chiffre saisi dans notre div resultat. On utilise "value" vu que nous sommes dans un input */
    } else if(button.substring(0,1) === "b") {
        manageOperation();
        switch(button){
            case "bPlus" : operateur = "+";
            break;
            case "bSous" : operateur = "-";
            break;
            case "bMult" : operateur = "*";
            break;
            case "bDiv" : operateur = "/";
            break;
            default :
            break;
        }
        if(reinit){
            recap = "";
            reinit = false;
        }
        if(nbCalculs > 1) recap += "<br />";
        recap += resultat + " " + operateur + " ";
    } else if(button === "point") {
        chiffreSaisi += ".";
    } else if(button === "egal") {
        manageOperation();
        recap += " = " + resultat;
        nbCalculs =1;
        reinit = true;
    }
    divRecap.innerHTML = recap;
    divRecap.scrollTop = divRecap.scrollHeight /* la taille du scroll */ - divRecap.clientHeight /* la taille de cet element */;
});

function manageOperation(){
    if(chiffreSaisi != ""){
        resultat = doOperation(operateur,resultat,parseFloat(chiffreSaisi)); /* parseFloat pour forcer la saisie d'un entier uniquement. Et donc pas de chaine de caracteres. */
    if(nbCalculs > 0){
        recap += parseFloat(chiffreSaisi);
    }
        divResultat.value = resultat;
        chiffreSaisi = ""; /* On reinitialise a vide le chiffre saisi pour une nouvelle operation */
        nbCalculs ++;
    } else {
        var position = recap.lastIndexOf("<br />"); /* lastIndexOf pour recuperer la derniere position de la chaine br, dans recap */
        recap = recap.substring(0,position); /* recuperer ancienne valeur et soustraire ce qu'il y a apres le br. (0,position) => que le debut */
    }
};

function doOperation(operateur,chiffreA,chiffreB){
    var res = 0;
    switch(operateur){
        case "+" : res = chiffreA + chiffreB;
        break;
        case "-" : res = chiffreA - chiffreB;
        break;
        case "*" : res = chiffreA * chiffreB;
        break;
        case "/" : res = chiffreA / chiffreB;
        break;
    }
    return res;
}