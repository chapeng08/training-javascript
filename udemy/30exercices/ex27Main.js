const inputPseudo = document.querySelector("#pseudo");
const inputChiffre = document.querySelector("#chiffre");
const buttonAddition = document.querySelector("#addition");
const buttonFactorielle = document.querySelector("#factorielle");
const resultat = document.querySelector("#resultat");


function afficherResultat(calcul){
    resultat.innerHTML = "<h1> Bonjour " + inputPseudo.value + "</h1>" ;
    resultat.innerHTML = "<div> Le resultat du calcul est : " + calcul + "</div>";
};

buttonAddition.addEventListener("click", function(){
    var calculResultat = calculs.addition(parseInt(inputChiffre.value));
    afficherResultat(calculResultat);
});

buttonFactorielle.addEventListener("click", function(){
    var calculResultat = calculs.factorielle(parseInt(inputChiffre.value));
    afficherResultat(calculResultat);
});