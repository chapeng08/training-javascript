var chien1 = creerAnimal("Pat", 8, false);
var chien2 = creerAnimal("kaka", 3, false);
var chien3 = creerAnimal("Pouf", 2, true);
var chat1 = creerAnimal("Katy", 18, false);
var chat2 = creerAnimal("Lava", 4, false);
var chat3 = creerAnimal("Billy", 1, false);
var chat4 = creerAnimal("Pit", 12, true);

var chiens = [];
chiens.push(chien1, chien2, chien3);

var chats = [];
chats.push(chat1, chat2, chat3, chat4);


afficherListeAnimal(chiens);
console.log(`La moyenne d'age des ${chiens.length} chiens est de : ${calculMoyenneAge(chiens)} ans. \n`);

afficherListeAnimal(chats);
console.log(`La moyenne d'age des ${chats.length} chats est de : ${calculMoyenneAge(chats)} ans.`);

function creerAnimal(nom, age, sexe){
    var animal = {};
    animal.nom = nom;
    animal.age = age;
    animal.sexe = sexe;
    return animal
}

function afficherListeAnimal(liste){
    var txt = "";
    for(var i = 0; i < liste.length; i++){
        txt += "Nom : " + liste[i].nom + "\n";
        txt += "Age : " + liste[i].age + "\n";
        if(liste[i].sexe){
            txt += "Sexe : Male\n";
        } else { 
            txt += "Sexe : Femelle\n";
    }
    txt += "------------------------------\n";
    }
    console.log(txt);
}


function calculMoyenneAge(liste){
    var moyenne = 0;
    for(var i = 0; i < liste.length; i++){
        moyenne += liste[i].age;
    }
    return moyenne/liste.length;
}
