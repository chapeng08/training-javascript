const myButton = document.querySelector("button");
const myDiv = document.querySelector("div");


// var results = ["pile", "face"]

// myButton.addEventListener("click", function(){
//     var randomResults = Math.floor(Math.random()*(results.length)); // arrayName.length permet de traiter plusieurs valeurs
//     myDiv.innerHTML = results[randomResults];
// });

myButton.addEventListener("click", function(){
    myDiv.innerHTML = pileOuface();
});

function pileOuface(){
    var random = Math.floor(Math.random() * 3);
    // if(random >= 1) return "face";
    // else return "pile";
    return (random >= 1) ? "face" : "pile";
};