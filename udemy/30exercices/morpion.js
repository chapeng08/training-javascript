var readline = require ("readline-sync");
var tour = 1; /* => a quel joueurr */
var fin = false; /* On itialise la valeur de fin a falsa pour dire qu'au demarrage le jeu n'est pas termine*/
var nbCellules = 9;


/* Construire un tableau a 2 dimensions
Creer d'abord 3 tableaux => a chaque ligne du tableau

var ligne1 = [0,0,0];               [0,0,0] => Tableau vide
var ligne2 = [0,0,0];
var ligne3 = [0,0,0];

Puis creer un tableau qui contient ces trois lignes 

var tab =[ligne1,ligne2,ligne3]; */

/* On peut aussi le faire plus rapidement : */

var morpion = [
    [0,0,0],
    [0,0,0],
    [0,0,0]
];


afficherGrille(morpion);

while(!fin && nbCellules > 0){
    var positionCorrecte = false;
    while(!positionCorrecte){
        console.log("----------------------------");
        console.log("Quelle position voulez-vous?");
        var saisieLigne = parseInt(readline.question("Ligne :"));
        var saisieColonne = parseInt(readline.question("Colonne :"));
        positionCorrecte = verifPosition(saisieLigne,saisieColonne,morpion);
        if(!positionCorrecte) console.log("************Choisir une position valide************");
    }
    
    morpion[saisieLigne - 1][saisieColonne - 1] = tour;
    nbCellules--;

    afficherGrille(morpion);
    
    fin = verifEndGame(morpion);
    if(fin){
        console.log("Bravo %d a gagne.", tour);
    }
    (tour === 1) ? tour = 2 : tour = 1; /* => if(tour === 1) tour = 2; else(tour = 1); */

}
if(nbCellules === 0){
    console.log("La partie est terminee. Personne n'a pas pu gagner.")
}

function afficherGrille(tab){
    /* On va maintenant parcourir chacun de nos elements du tableau via 2 boucles.*/
    for(let i = 0; i < tab.length; i++){ /* Une premiere pour chaque ligne (i < tab.length) */
        var txt = "";
        for(let j = 0; j < tab[i].length; j++){ /* puis une seconde pour chaque element contenu dans chaque ligne (j < tab[i].length) */
            if(tab[i][j] === 0) txt += "| |";
            else if(tab[i][j] === 1) txt += "|x|";
            else if(tab[i][j] === 2) txt += "|o|";
        }
        console.log(txt);
    }
};

function verifPosition(saisieLigne,saisieColonne,morpion){
    if(saisieLigne >= 0 && saisieLigne <= 3 && saisieColonne >= 0 && saisieColonne <= 3 && morpion[saisieLigne-1][saisieColonne-1] === 0)
    return true;
    else return false;
};

function verifEndGame(tab){
    for(let i = 0; i < tab.length; i++){
        /* Verifier si un des deux joueurs a gagne sur une ligne */
        if(tab[i][0] === tab[i][1] && tab[i][0] === tab[i][2] && tab[i][0] !== 0) return true;
        /* Verifier si un des deux joueurs a gagne sur une colonne */
        if(tab[0][i] === tab[1][i] && tab[0][i] === tab[2][i] && tab[0][i]) return true;
    }
        /* Verifier si un des deux joueurs a gagne sur les deux diagonales */
        if(tab[0][0] === tab[1][1] && tab[0][0] === tab[2][2] && tab[0][0] !== 0) return true;
        if(tab[0][2] === tab[1][1] && tab[0][2] === tab[2][0] && tab[0][2] !== 0) return true;
}