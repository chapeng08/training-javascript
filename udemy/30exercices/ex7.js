//Inscrire les 2 variables en Input
var c1 = 123;
var c2 = 20;

// Creation de la fonction qui doit realiser un affichage
afficheResultatDivisionPar3(c1);
afficheResultatDivisionPar3(c2);

// Declaration la fonction
function afficheResultatDivisionPar3(chiffre){
    if (chiffre % 3 === 0) { //Modulo % verifie la division euclidienne par 3
        console.log("Le resultat de " + chiffre + " / 3 = " + chiffre / 3);
    } else {
        console.log("le chiffre " + chiffre + " n'est pas divisible par 3. ");
    }    
}