var readline = require ("readline-sync");

var arr = [3, 5, 10, 12];
var saisie1 = "";


while(saisie1 != "q" && saisie1 != "Q"){
    menu()
    var saisie1 = readline.question("Quel est votre choix ? ");
    switch(saisie1){
        case "a" :
        case "A" : ajoutChiffre();
        break;
        case "b" :
        case "B" : afficheArr();
        break;
        case "q" :
        case "Q" : console.log("A + ");
        break;
        default : console.log("Je n'ai pas compris");
    };
}

function menu(){
    var txt = "";
    txt += "A/ Ajouter un chiffre \n";
    txt += "B/ Afficher le tableau \n";
    txt += "Q/ Quitter le programme";
    console.log(txt);
}

function ajoutChiffre(){
    var saisie2 = parseInt(readline.question("Quel chiffre a ajouter ? "));
    var positionToPush = getPosition(arr, saisie2);
    arr.splice(positionToPush,0,saisie2);
}

function getPosition(ar,num){
    for(var i = 0; i < ar.length; i++){
        if(ar[i] > num){
            return i;
        }
    }
    return ar.length;
}

function afficheArr(){
    console.log(arr);
}