const login = document.querySelector("#login");
const password = document.querySelector("#password");
const resultatpassword = document.querySelector("#resultatPassword");



login.addEventListener("keyup",function(){
    var regex = /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/
    if(!regex.test(login.value)){
        login.style.backgroundColor = "red";
    } else {
        login.style.backgroundColor = "green";
    }
});

password.addEventListener("blur", function(){
    var caractDec = /\d/;
    var caractSpec = /[$&@!]/;
    var error = "";
    
    if(password.value.length < 6){
        error += "<li>est trop court</li>";
    } else if(password.value.length > 6){
        error += "<li>est trop long</li>";
    }
    
    if(!password.value.match(caractSpec)){
        error += "<li>doit contenir un caractere special $ & @ ou !</li>"
    }
    
    if(!password.value.match(caractDec)){
        error += "<li>doit contenir un chiffre</li>"
    }

    if(error !== ""){
        resultatpassword.innerHTML = "Le password doit contenir: <ul>" + error + "</ul>."
        resultatPassword.style.border = "2px solid red" ;
    } else {
        resultatpassword.innerHTML = "Le password est valide."
        resultatPassword.style.border = "2px solid green" ;
    }
});