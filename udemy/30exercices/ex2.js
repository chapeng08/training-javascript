var Nom = 'Toto';
var Age = 30;
var Homme = true;

/*

console.log("Nom : " + Nom);
console.log("Age : " + Age);
if (Homme === true){
    console.log("Sexe : Homme");
}
else {
    console.log("Sexe : Femme");
};

*/

console.log("Nom : %s ", Nom); // %s : string, permet d'inserer des valeurs a l'interieur d'une chaine de caractere.
console.log("Age : %d ", Age); // %d : decimal
if (Homme){
    console.log("Sexe : Homme");
}
else {
    console.log("Sexe : Femme");
};


/*

console.log("%s a %d ans ",Nom,Age);
if (Homme){
    console.log("Sexe : Homme");
}
else {
    console.log("Sexe : Femme");
};

*/