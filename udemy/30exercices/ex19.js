var readline = require ("readline-sync");

var saisie = parseInt(readline.question("Saisir un chiffre : "));
var result = 1;

for(let i = 1; i <= saisie; i++){ // Chemin normal
    result *= i; // <=> result = result * i
    console.log("Etape " + i + " : " + result);
}

console.log("Le resultat de la factorielle " + saisie + " est " + result);

// ********

// var result = 1;
// for(let i = saisie; i >= 1; i--){ // Chemin inverse
//     result *= i; // <=> result = result * i
//     console.log("Etape " + i + " : " + result);
// }

// console.log("Le resultat de la factorielle " + saisie + " est " + result);

// ******

// var result = factorielle(saisie);

// function factorielle(n){
//     if(n > 1){
//         return n * factorielle(n-1)
//     } else {
//         return 1;
//     }
// }

// console.log("Le resultat de la factorielle " + saisie + " est " + result);

