var readline = require ("readline-sync");
var fs = require("fs");
var types = require ("./ex28Types.json");
var armes = require ("./ex28Armes.json");

var armesManager = {
    afficherArmes : function(){
        /* Pour afficher les armes, on doit toutes les parcourir via une boucle */
        for(var arme in armes){
            if(arme.substr(0,4) === "arme"){
        /* On va devoir recuperer l'id de chaque arme, c'est a dire le dernier caractere dans notre fichier ex28Armes.js 
        (par exemple: arme1) avec un substr*/    
            var idArme = arme.substr(-1,1); /* (-1,1) pour recuperer le dernier caractere de notre chaine de caractere.*/
            console.log(idArme + " : " + this.afficherArme(armes[arme])); /* Je declare ici une nouvelle fonction pour
            l'affichage de chaque ame*/
        }
    }
    },

    afficherArme : function(arme){
        return ("Nom : " + arme.nom + " - Type : " + types[arme.type]);
    },

    ajouterArme : function(){
        var nouvelleArme = {};
        nouvelleArme.nom = readline.question("Quel est le nom de la nouvelle arme ?");
        /*On va devoir afficher a l'user l'ensemble des types afin que l'user choisisse le type voulu, 
        cela en creant une nouvelle fonction*/
        this.afficherTypes();
        nouvelleArme.type = parseInt(readline.question("Quel est son type ?"));
        armes.increment++;
        armes["arme" + armes.increment] = nouvelleArme;
        fs.writeFileSync("./ex28Armes.json", JSON.stringify(armes));    
    },


    afficherTypes : function(){
        for(var type in types){
            console.log(type + " : " + types[type]); /* conteneur[id de l'objet] */
        }
    },

    supprimerArme : function(){    
        var armeAsupprimer = readline.question("Quel est le numero de l'arme a supprimer ?");
        delete armes["arme" + armeAsupprimer];
        fs.writeFileSync("./ex28Armes.json", JSON.stringify(armes));
    },

}

module.exports = armesManager;