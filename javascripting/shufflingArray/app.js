let arry = [1, 2, 3, 4]

let arrayShuffle = function(arr) {
    let newPosition,
        temp;

    for (let i = arr.length - 1; i > 0; i--) {
        newPosition = Math.floor(Math.random() * (i + 1));
        temp = arr[i];
        arr[i] = arr[newPosition];
        arr[newPosition] = temp;
    }
    return arr;
};

let newArray = arrayShuffle(arry);

console.log(newArray);

let newArray2 = arrayShuffle(newArray);

console.log(newArray2);