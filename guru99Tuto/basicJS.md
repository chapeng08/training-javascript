JavaScript is a client-side scripting language developed by Brendan Eich. <br />
JavaScript can be run on any operating systems and almost all web browsers. <br />
You need a text editor to write JavaScript code and a browser to display your web page. <br />


### A Simple JavaScript Program #
You should place all your JavaScript code within <script> tags (<script> and </script>) if you are keeping your JavaScript code within the HTML document itself. This helps your browser distinguish your JavaScript code from the rest of the code. As there are other client-side scripting languages (Example: VBScript), it is highly recommended that you specify the scripting language you use.  You have to use the type attribute within the <script> tag and set its value to text/javascript like this:
```
<script type="text/javascript">
```

