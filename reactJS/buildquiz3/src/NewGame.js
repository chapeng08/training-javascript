import React from 'react';
import {Link} from 'react-router-dom';

function NewGame() {
    return (
    <div className="Container" >
        <Link to='/App'>
        <button className="btn btn-primary">New game</button>
        </Link>
    </div>
    );
}

export default NewGame;