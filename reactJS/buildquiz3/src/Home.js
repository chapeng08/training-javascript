import React from 'react';
import './App.css';
import App from './App';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import NewGame from './NewGame';

function Home() {
    return (
        <BrowserRouter> {/* makes pretty URLs like example.com/about */}
            <div className="container" >
                <h1>BJJ Quiz</h1>
                <Switch> {/*Groups all the routes together, and ensures they take precedence from top-to-bottom*/}
                    <Route path="/" exact component={NewGame}/> {/*Each individual route*/}
                    <Route path="/App" component={App}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default Home;