# Using React Router for a Single Page Application #

React doesn't come with a built-in router, but we can easily achieve routing with the react-router-dom library. Routing is how a web applications direct traffic.  <br />
A Single Page Application (or SPA) - only one page is loaded, and every click to a new page loads some additional JSON data, but does not actually request a new resource like loading index.html and about-me.html would.


## Installation #

```
npm install react-router-dom
```


## Browser Router

To use react-router-dom, we need to wrap our entire App component in BrowserRouter. There are two types of routers:
- BrowserRouter - makes pretty URLs like example.com/about.
- HashRouter - makes URLs with the octothorpe (or hashtag, if you will) that would look like example.com/#about. <br />
Let's use BrowserRouter. <br />
**src/index.js**
```
import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.querySelector('#root')
)
```


## Route and Switch #

Now in App.js, we can decide on the routes we want to use and direct accordingly. We'll use Route and Switch for this task.
- Switch: Groups all your routes together, and ensures they take precedence from top-to-bottom.
- Route: Each individual route.

**App.js**
```
import React from 'react'
import { Route, Switch } from 'react-router-dom'
// We will create these two pages in a moment
import HomePage from './pages/HomePage'
import UserPage from './pages/UserPage'

export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/:id" component={UserPage} />
    </Switch>
  )
}
```
We're matching the root route (/) to HomePage, and dynamically matching any other page to UserPage.


## Link #

In order to link to a page within the SPA, we'll use Link. If we used the traditional <a href="/route">, it would make a completely new request and reload the page, so we have Link to help us out.

**Homepage.js**
```
import React, { Component } from 'react'
import { Link } from 'react-router-dom'

export default function HomePage() {
  return (
    <div className="container">
      <h1>Home Page</h1>
      <p>
        <Link to="/taniarascia">taniarascia</Link> on GitHub.
      </p>
    </div>
  )
}
```