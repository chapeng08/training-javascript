import React, {useReducer} from 'react';
import Question from './components/Question';
import Answers from './components/Answers';
import QuizContext from './context/QuizContext';


import {
    SET_ANSWERS,
    SET_CURRENT_QUESTION,
    SET_CURRENT_ANSWER,
    SET_ERROR,
    SET_SHOW_RESULTS,
    RESET_QUIZ,
} from './reducers/types.js';
import quizReducer from './reducers/QuizReducer';

import './App.css';


function App() {
    const questions = [
        {
            id: 1,
            question: 'A triangle choke is a form of',
            answer_a: 'A strangulation choke',
            answer_b: 'A blood choke',
            answer_c: 'An anaconda choke',
            answer_d: 'A darce choke',
            correct_answer: 'b',
        },
        {
            id: 2,
            question: 'When you chicken wing your opponents arm and provide torque to his shoulder with your arms, that move is called...',
            answer_a: 'Key lock',
            answer_b: 'Omoplata',
            answer_c: 'Kimura',
            answer_d: 'Americana',
            correct_answer: 'c',
        },
        {
            id: 3,
            question: 'What are the three pillars of jiu jitsu?',
            answer_a: 'Triangle, kneebar, kimura',
            answer_b: 'Armbar, americana, omoplata',
            answer_c: 'Armbar, triangle, omoplata',
            answer_d: 'Omoplata, gogoplata, monoplata',
            correct_answer: 'c',
        },
        {
            id: 4,
            question: 'Which position can you not get a kneebar from?',
            answer_a: 'Half guard top',
            answer_b: 'Half guard bottom',
            answer_c: 'Bottom mount',
            answer_d: 'Bottom guard',
            correct_answer: 'c',
        },
        {
            id: 5,
            question: 'What is not a shoulder lock?',
            answer_a: 'Armbar',
            answer_b: 'Americana',
            answer_c: 'Kimura',
            answer_d: 'Omoplata',
            correct_answer: 'a',
        },
    ];

    const initialState = {
        questions,
        currentQuestion: 0,
        currentAnswer: '',
        answers: [],
        showResults: false,
        error: '',
    };

    const [state, dispatch] = useReducer(quizReducer, initialState);
    const {currentQuestion, currentAnswer, answers, showResults, error} = state;

    const question = questions[currentQuestion];

    const renderError = () => {
        if (!error) {
            return;
        }

        return <div className="error">{error}</div>;
    };

    const renderResultMark = (question, answer) => {
        if (question.correct_answer === answer.answer) {
            return <span className="correct">Correct</span>;
        }

        return <span className="failed">Failed</span>;
    };

    const renderResultsData = () => {
        return answers.map(answer => {
            const question = questions.find(
                question => question.id === answer.questionId
            );

            return (
                <div key={question.id}>
                    {question.question} - {renderResultMark(question, answer)}
                </div>
            );
        });
    };

    const showResult = () => {
        if (currentQuestion < 1){
            dispatch({type: SET_ERROR, error: 'Error: Please select an answer to show the result'});
        } else {
            dispatch({type: SET_SHOW_RESULTS, showResults: true});
        }

    }

    const restart = () => {
        dispatch({type: RESET_QUIZ});
    };

    const next = () => {
        const answer = {questionId: question.id, answer: currentAnswer};

        if (!currentAnswer) {
            dispatch({type: SET_ERROR, error: 'Error: Please select an answer to continue'});
            return;
        }

        answers.push(answer);
        dispatch({type: SET_ANSWERS, answers});
        dispatch({type: SET_CURRENT_ANSWER, currentAnswer: ''});
        
        if(currentQuestion +1 < questions.length){
            dispatch({
                type: SET_CURRENT_QUESTION,
                currentQuestion: currentQuestion + 1,
            });
            return;
        }

        dispatch({type: SET_SHOW_RESULTS, showResults: true});
    };

    if (showResults) {
        return (
            <div className="container results">
                <h2>Results</h2>
                <ul>{renderResultsData()}</ul>
                <button className="btn btn-primary" onClick={restart}>
                    Restart the quiz
                </button>
            </div>
        );
      } else {
        return (
            <QuizContext.Provider value={{state, dispatch}}>
                <div className="container">
                    <Question />
                    {renderError()}
                    <Answers />
                    <button className="btn btn-primary" onClick={next}>
                        Confirm and Continue
                    </button>
                    <button className="btn btn-primary" onClick={showResult}>
                        Quit and show results
                    </button>
                </div>
            </QuizContext.Provider>
        );
    }
}

export default App;