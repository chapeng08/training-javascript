import React, { Component } from 'react';
import quizQuestions from './api/quizQuestions';
import Quiz from './components/Quiz';
import './App.css';
 
class App extends Component {
    constructor(props) {
    super(props);
 
    this.state = {
      questionId: 1,
      question: '',
      answerOptions: [],
      answer: '',
      result: ''
    };
 
    this.handleAnswerSelected = this.handleAnswerSelected.bind(this);
  }
 
  componentWillMount() {
    const shuffledAnswerOptions = quizQuestions.map((question) => this.shuffleArray(question.answers));
    this.setState({
      question: quizQuestions[0].question,
      answerOptions: shuffledAnswerOptions[0]
    });
  }
 
  shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
 
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
 
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
 
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
 
    return array;
  };
 
  handleAnswerSelected() { 
    if (this.state.questionId < quizQuestions.length) {
        setTimeout(() => this.setNextQuestion(), 300);
    } else {
        setTimeout(() => this.setResults(this.getResults()), 300);
    }
  }
 
 
  setNextQuestion() {
    const questionId = this.state.questionId + 1;
 
    this.setState({
        questionId: questionId,
        answer: ''
    });
  }
 
  getResults() {
    
  }
 
  setResults(result) {
  }
 
  renderQuiz() {
    return (
      <Quiz
        answer={this.state.answer}
        answerOptions={this.state.answerOptions}
        questionId={this.state.questionId}
        question={this.state.question}
        questionTotal={quizQuestions.length}
        onAnswerSelected={this.handleAnswerSelected}
      />
    );
  }
 
  renderResult() {
    
  }
 
  render() {
    return (
      <div className="App">
        <div className="App-header">          
        </div>
        {this.state.result ? this.renderResult() : this.renderQuiz()}
      </div>
    );
 
 
 
 
  }
 
}
 
export default App;
