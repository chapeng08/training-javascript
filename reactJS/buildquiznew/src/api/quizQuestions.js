var quizQuestions = [
    {
        id: 1,
        question: 'A triangle choke is a form of',
        answer_a: 'A strangulation choke',
        answer_b: 'A blood choke',
        answer_c: 'An anaconda choke',
        answer_d: 'A darce choke',
        correct_answer: 'b',
    },
    {
        id: 2,
        question: 'When you chicken wing your opponents arm and provide torque to his shoulder with your arms, that move is called...',
        answer_a: 'Key lock',
        answer_b: 'Omoplata',
        answer_c: 'Kimura',
        answer_d: 'Americana',
        correct_answer: 'c',
    },
    {
        id: 3,
        question: 'What are the three pillars of jiu jitsu?',
        answer_a: 'Triangle, kneebar, kimura',
        answer_b: 'Armbar, americana, omoplata',
        answer_c: 'Armbar, triangle, omoplata',
        answer_d: 'Omoplata, gogoplata, monoplata',
        correct_answer: 'c',
    },
    {
        id: 4,
        question: 'Which position can you not get a kneebar from?',
        answer_a: 'Half guard top',
        answer_b: 'Half guard bottom',
        answer_c: 'Bottom mount',
        answer_d: 'Bottom guard',
        correct_answer: 'c',
    },
    {
        id: 5,
        question: 'What is not a shoulder lock?',
        answer_a: 'Armbar',
        answer_b: 'Americana',
        answer_c: 'Kimura',
        answer_d: 'Omoplata',
        correct_answer: 'a',
    },
];

export default quizQuestions;
