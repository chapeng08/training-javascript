import React from 'react';



import './App.css';


class App extends React.Component {
  
 constructor(props) {
    super(props);
 
    this.state = {
      questionId: 1,
      question: '',
      answerOptions: [],
      answer: '',
      result: '',
      error: ''
    };
 
}


shuffleArray(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};


  render() {
    return (
      <div className="container">
        <h1>BJJ Quiz</h1>
        <button className="btn btn-primary" onClick="">
        Confirm and Continue
        </button>
        <button className="btn btn-primary" onClick="">
        Quit and show results
        </button>
      </div>
    );
  }
 
}

export default App;
