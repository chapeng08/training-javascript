JavaScript | Math.pow( ) Function

The Math.pow() function in JavaScript is used to power of a number i.e., the value of number raised to some exponent. Since, the Math.pow() is a static method of Math and therefore it is always used as Math.pow() and not as a method of an object created of Math class.

Syntax:

Math.pow(base, exponent)

Parameters:

    base: It is the base number which is to be raised.
    exponent: It is the value used to raise the base.

Return Value: The Math.pow() function returns a number representing the given base raised to the power of the given exponent.

Below examples illustrates the Math.pow() function in JavaScript:

    When both base and exponent are passed as positive numbers in parameters:
    filter_none

    brightness_4
    <script type="text/javascript"> 
        document.write(Math.pow(9, 3)); 
    </script> 

    Output:

    729

    When the base value is negative and the exponent is positive:
    filter_none

    brightness_4
    <script type="text/javascript"> 
        document.write(Math.pow(-9, 3)); 
    </script> 

    Output:

    -729

    When the base value is positive and the exponent is negative:
    filter_none

    brightness_4
    <script type="text/javascript"> 
        document.write(Math.pow(-9, 3)); 
    </script> 

    Output:

    0.0013717421124828531

    When the base value is negative and the exponent has decimal point:
    filter_none

    brightness_4
    <script type="text/javascript"> 
        document.write(Math.pow(-9, 0.5)); 
    </script> 

    Output:

    NaN


